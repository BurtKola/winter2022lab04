import java.util.Scanner;

public class Application{
	public static void main(String[] args){
		Scanner reader = new Scanner(System.in);
		
		Album[] myCollection = new Album[4];
		
		for(int i=0;i<myCollection.length;i++){
			
			// setter method version
			/*
			myCollection[i] = new Album();
			
			System.out.println("Enter album title.");	
			myCollection[i].setTitle(reader.nextLine());
			
			System.out.println("Enter album genre.");
			myCollection[i].setGenre(reader.nextLine());
			
			System.out.println("Enter album release year.");
			myCollection[i].setReleaseYear(Integer.parseInt(reader.nextLine()));
			*/
			
			// constructor version
			
			System.out.println("Enter album title.");
			String tempTitle = reader.nextLine();
			
			System.out.println("Enter album genre.");
			String tempGenre = reader.nextLine();
			
			System.out.println("Enter album release year.");
			int tempReleaseYear = Integer.parseInt(reader.nextLine());
			
			myCollection[i] = new Album(tempTitle, tempGenre, tempReleaseYear);
			
		}
		// print BEFORE setter call
		System.out.println(myCollection[3].getTitle());
		System.out.println(myCollection[3].getGenre());
		System.out.println(myCollection[3].getReleaseYear());
		
		System.out.println("Enter album title.");	
		myCollection[3].setTitle(reader.nextLine());
		
		System.out.println("Enter album genre.");
		myCollection[3].setGenre(reader.nextLine());
		
		System.out.println("Enter album release year.");
		myCollection[3].setReleaseYear(Integer.parseInt(reader.nextLine()));
		
		// print AFTER setter call
		System.out.println(myCollection[3].getTitle());
		System.out.println(myCollection[3].getGenre());
		System.out.println(myCollection[3].getReleaseYear());
	}
}