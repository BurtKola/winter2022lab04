public class Album{
	private String title;
	private String genre;
	private int releaseYear;
	
	public void setTitle(String title){
		this.title = title;
	}
	public void setGenre(String genre){
		this.genre = genre;
	}
	public void setReleaseYear(int releaseYear){
		this.releaseYear = releaseYear;
	}
	
	public String getTitle(){
		return title;
	}
	public String getGenre(){
		return genre;
	}
	public int getReleaseYear(){
		return releaseYear;
	}
	
	
	public Album(String title, String genre, int releaseYear){
		this.title = title;
		this.genre = genre;
		this.releaseYear = releaseYear;
	
	}
	
	public void albumAge(){
		int age=2022-releaseYear;
		System.out.println(title+ " is " +age+ " years old!");
	}
}